import * as path from "path";
import { browser, Config } from "protractor";
import { Reporter } from "../support/reporter";
const jsonReports = process.cwd() + "/reports/json";

export const config: Config = {

    seleniumAddress: "http://127.0.0.1:4444/wd/hub",

    SELENIUM_PROMISE_MANAGER: false,
    setDefaultTimeout : 50000, //For Handling the default timeout for a test
    // directConnect : true,
    baseUrl: "https://www.google.com",

    // capabilities: {
    //     browserName: "firefox",
    // },

    Multicapabilities:[ {
        browserName: "chrome"
    },
       { browserName: "firefox"
    }
],

    framework: "custom",
    frameworkPath: require.resolve("protractor-cucumber-framework"),

    specs: [
        "../../features/evalCucumber.feature",
    ],

    suites:{
        "cucumber" : "../../features/evalCucumber.feature",
        "protractor" : "../../cucumberfeatures/evalProtractor.feature"
},
    onPrepare: () => {
        browser.ignoreSynchronization = true;
        browser.manage().window().maximize();
        Reporter.createDirectory(jsonReports);
    },

    cucumberOpts: {
        compiler: "ts:ts-node/register",
        format: "json:./reports/json/cucumber_report.json",
        require: ["../../typeScript/stepdefinitions/*.js", "../../typeScript/support/*.js"],
        strict: true,
        // tags: "@CucumberScenario or @ProtractorScenario or @TypeScriptScenario or @OutlineScenario",
         tags: "@CucumberScenario",

    },

    onComplete: () => {
        Reporter.createHTMLReport();
    },
};
